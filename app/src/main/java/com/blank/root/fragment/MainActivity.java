package com.blank.root.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button login,register;
    FragmentManager fm = getSupportFragmentManager();
    FragmentTransaction transaction;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        toolbar = (Toolbar)findViewById(R.id.toolbar);

        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register);
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        //inisial fragment
        transaction = fm.beginTransaction();
        //memmanggil fragmnet login
        LoginFragment login = new LoginFragment();
        //Add / replace /remove
        transaction.add(R.id.container, login);
        transaction.commit();
    }

    public void gantiFragment(Fragment fragment){
        transaction = fm.beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }
    @Override
    public void onClick(View v) {
        if(v == register){
            RegisterFragment register = new RegisterFragment();
            gantiFragment(register);
        }
        if(v == login){
            LoginFragment login = new LoginFragment();
            gantiFragment(login);
        }
    }
}
